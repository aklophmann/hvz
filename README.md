# human vs Zombie game - Backend


## Git structure

Branching name standard
yourName-objectiveOfBranch

\Master 
	- only when deployment is ready are changes allowed

\develop
	- integration level merging

\api
	- main api branch


## Testing - JUnit
Perform initial testing, testing out TDD.

## Task managment

- [ ] Game class and controller - Andreas
- [ ] User class and controller - Alex
- [ ] Player class and controller - Eirik
- [ ] Squad class and controller - Sadegh
- [ ] Mission class and controller - Truc-My
- [ ] Kill class and controller - Eirik
- [ ] Chat class and controller - Alex
- [ ] SquadMember class and controller - Sadegh
- [ ] SquadCheckIn class and controller - Sadegh



