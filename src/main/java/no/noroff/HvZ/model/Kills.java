package no.noroff.HvZ.model;


import com.fasterxml.jackson.annotation.*;
import no.noroff.HvZ.config.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
@Table
public class Kills {

    @JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kill_id")
    public int id;

    @NotNull
    @Column(name = "time_of_death")
    public LocalTime timeOfDeath;

    @Column(name = "story")
    public String story;

    @Column(name = "lat")
    public double lat;

    @Column(name = "lng")
    public double lng;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="game_id"))
    public int gameId;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="killer_id"))
    public int killerId;

    @JsonIgnoreProperties(value = {"killed", "killsList"})
    @OneToOne(orphanRemoval = false)
    @JoinColumn(nullable = false, name="victim_id", referencedColumnName = "player_id")
    public Player victim;

    public Kills() {}

    public Kills(int id) {
        this.id = id;
        timeOfDeath = LocalTime.of(15,40);
        story = "he ded";
        gameId = 1;
        killerId = 1;
        //victimId = 3;

    }

}
