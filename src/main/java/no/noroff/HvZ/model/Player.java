package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import no.noroff.HvZ.config.View;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "game_id"}))
public class Player {

    //@JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id")
    public int id;

    @NotNull
    @Column(name = "username")
    public String username;

    //@JsonView(View.User.class)
    @NotNull
    @Column(name = "is_human")
    public boolean isHuman;


    @JsonView(View.Admin.class)
    @NotNull
    @Column(name = "is_patient_zero")
    public boolean isPatientZero;


    @Column(name = "squad_id")
    public int squadId;


    //@JsonView(View.Admin.class)
    @NotNull
    @Column(name = "bite_code", unique = true)
    public int biteCode;

    //@JsonView(View.User.class)
    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="game_id"))
    @Column(name = "game_id")
    public int gameId;

    @JsonView(View.Admin.class)
    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name="user_id"))
    @Column(name="user_id")
    public int userId;

    @OneToMany(mappedBy = "killerId", orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Kills> killsList;

    @JsonIgnoreProperties("victim")
    @OneToOne(mappedBy = "victim", orphanRemoval = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    public Kills killed;

    @JsonIgnoreProperties("squadMember")
    @OneToOne(mappedBy = "squadMember", orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public SquadMember squadMember;

    @OneToMany(mappedBy = "playerId", cascade = CascadeType.ALL, orphanRemoval = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Chat> chatList;

}
