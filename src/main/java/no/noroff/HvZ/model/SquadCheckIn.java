package no.noroff.HvZ.model;

import com.fasterxml.jackson.annotation.JsonView;
import no.noroff.HvZ.config.View;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class SquadCheckIn {

    @JsonView(View.Admin.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_checkin_id")
    public int id;

    @NotNull
    @Column(name = "start_time")
    public java.sql.Time startTime;

    @NotNull
    @Column(name = "end_time")
    public java.sql.Time endTime;

    @NotNull
    @Column(name = "lat")
    public double lat;

    @NotNull
    @Column(name = "lng")
    public double lng;

    // addition to original ERD, tag the checkIn with an objective
    @NotNull
    @Column(name = "objective_tag")
    public String objectiveTag;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "game_id"))
    public int gameId;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "squad_id"))
    public int squadId;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "squad_member_id"))
    public int squadMemberId;
}
