package no.noroff.HvZ.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Squad{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_id")
    public int id;

    @Column(name = "squad_name")
    public String squadName;

    @JoinColumn(nullable = false, foreignKey = @ForeignKey(name = "game_id"))
    public int gameId;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "squadId", orphanRemoval = true)
    public List<SquadMember> squadMemberList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "squadId", orphanRemoval = true)
    public List<SquadCheckIn> squadCheckInList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "squadId", orphanRemoval = true)
    public List<Chat> chatList;
}
