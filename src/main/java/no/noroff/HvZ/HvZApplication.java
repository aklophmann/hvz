package no.noroff.HvZ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HvZApplication {

	public static void main(String[] args) {
		SpringApplication.run(HvZApplication.class, args);
	}

}
