package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Game;
import no.noroff.HvZ.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GameController {

    private final Game game_error = new Game();

    @Autowired
    private final GameRepository gameRepository;

    public GameController(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    // Create
    @CrossOrigin
    @PostMapping("/game")
    public ResponseEntity postGame(@RequestBody Game game){
        game.gameState = "Registration";
        gameRepository.save(game);
        return ResponseEntity.ok(game);
    }

    // Read
    @CrossOrigin
    @GetMapping("/game")
    public ResponseEntity getAllGames() { return ResponseEntity.ok(gameRepository.findAll()); }

    @CrossOrigin
    @GetMapping("/game/{id}")
    public ResponseEntity<Game> getGameById(@PathVariable(name = "id") int id) {
        Game game = gameRepository.findGameById(id);

        if (game != null) return new ResponseEntity<>(game, HttpStatus.OK);
        return new ResponseEntity<>(game_error, HttpStatus.BAD_REQUEST);
    }

    // Update
    @CrossOrigin
    @PutMapping("/game/{id}")
    public Game updateGame(@PathVariable(name = "id") int id, @RequestBody Game game){
        System.out.println(game.latitude);
        Game prevGame = gameRepository.findGameById(id);
        prevGame.name = game.name;
        prevGame.gameState = game.gameState;
        prevGame.description = game.description;
        prevGame.startDate = game.startDate;
        prevGame.latitude = game.latitude;
        prevGame.longitude = game.longitude;
        return gameRepository.save(prevGame);
    }

    // Delete
    @CrossOrigin
    @DeleteMapping("/game/{id}")
    public ResponseEntity deleteGame(@PathVariable(name = "id") int id) {
        Game game = gameRepository.findGameById(id);
        gameRepository.delete(game);
        return ResponseEntity.ok(game);
    }


    //should be deleted?
    @CrossOrigin
    @GetMapping("/game/add/seed")
    public Game addGameSeed(){
        Game g = new Game();
        g.name = "HvZ Drammen";
        g.gameState = "Registration";
        g.is_north_west = true;
        g.latitude = 59.74;
        g.longitude = 10.18;

        return gameRepository.save(g);

    }
}
