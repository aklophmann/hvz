package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.User;
import no.noroff.HvZ.payload.ApiResponse;
import no.noroff.HvZ.repository.UserRepository;
import no.noroff.HvZ.security.JwtTokenProvider;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Arrays;

@RestController
public class UserController {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final User user_error = new User();

    UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @CrossOrigin
    @GetMapping(path="/game/users")
    public ResponseEntity showUsers(){
            return ResponseEntity.ok(userRepository.findAll());
    }

    @CrossOrigin
    @GetMapping(path="/game/users/{user_id}/")
    public ResponseEntity getSpecificUser(@PathVariable int user_id, Authentication authentication){

        int currentUserId = Math.toIntExact(((UserPrincipal) authentication.getPrincipal()).getId());

        if(currentUserId == user_id || Arrays.toString(authentication.getAuthorities().toArray()).equals("[ROLE_ADMIN]")){
            User user = userRepository.findUserById(user_id);
            return ResponseEntity.ok(user);
        }else{

            return ResponseEntity.badRequest().body(user_error);
        }
    }

    @CrossOrigin
    @PostMapping("/user/register")
    public ResponseEntity registerUser(@Valid @RequestBody User user) {
        if(userRepository.existsByUsername(user.username)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(user_error);
        }

        user.password = (passwordEncoder.encode(user.password));
        user.role = "ROLE_USER";

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.username).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }

    @CrossOrigin
    @PostMapping("/user/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody User user) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        user.username,
                        user.password
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        User u = userRepository.findByUsername(user.username);

        String jwt = tokenProvider.generateToken(authentication);
        Object[] obj = {jwt, u};
        return ResponseEntity.ok(obj);
    }

    @CrossOrigin
    @PostMapping("/user/{user_id}/makeAdmin")
    public ResponseEntity<?> authenticateUser(@PathVariable(name = "user_id") int userId, Authentication authentication) {

        if(Arrays.toString(authentication.getAuthorities().toArray()).equals("[ROLE_ADMIN]")) {
            User user = userRepository.findUserById(userId);
            user.role = "ROLE_ADMIN";
            return ResponseEntity.ok(userRepository.save(user));
        }

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(user_error);
    }
}
