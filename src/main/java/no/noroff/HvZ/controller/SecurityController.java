package no.noroff.HvZ.controller;


import no.noroff.HvZ.model.User;
import no.noroff.HvZ.repository.UserRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SecurityController {

    @Autowired
    UserRepository userRepository;

    @GetMapping(value = "/authentication/role")
    public ResponseEntity currentUserName(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        return ResponseEntity.ok(userRepository.findByUsername(userPrincipal.getUsername()));
    }
}