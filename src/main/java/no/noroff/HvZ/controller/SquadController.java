package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Chat;
import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.model.Squad;
import no.noroff.HvZ.model.SquadMember;
import no.noroff.HvZ.repository.ChatRepository;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.repository.SquadMemberRepository;
import no.noroff.HvZ.repository.SquadRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class SquadController {

    @Autowired
    private final SquadRepository squadRepository;

    @Autowired
    private final SquadMemberRepository squadMemberRepository;

    @Autowired
    private final PlayerRepository playerRepository;

    @Autowired
    private final ChatRepository chatRepository;

    private final Squad squad_error = new Squad();


    public SquadController(SquadRepository squadRepository, SquadMemberRepository squadMemberRepository, PlayerRepository playerRepository, ChatRepository chatRepository) {
        this.squadRepository = squadRepository;
        this.squadMemberRepository = squadMemberRepository;
        this.playerRepository =  playerRepository;
        this.chatRepository = chatRepository;
    }


    @CrossOrigin
    @GetMapping("/game/squad/test/{id}")
    public Squad test_stuff(@PathVariable int id){
        Squad squad1 = new Squad();
        squad1.id = 0;
        squad1.gameId = id;
        squad1.squadName = "test2";
        squadRepository.save(squad1);
        return squad1;
    }


    @CrossOrigin
    @GetMapping("/game/{game_id}/squad/{squad_id}")
    public ResponseEntity getSquad(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId) {
        Squad squad = squadRepository.findSquadBygameIdAndId(gameId, squadId);

        if (squad != null) return ResponseEntity.status(HttpStatus.OK).body(squad);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squad_error);
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/squad")
    public ResponseEntity getAllSquads(@PathVariable(name = "game_id") int gameId) {
        List<Squad> squadList = squadRepository.findSquadBygameId(gameId);
        return ResponseEntity.status(HttpStatus.OK).body(squadList);
    }

    // Needs to make on squad-init a player a squadMember and rank it
    // automatically set a player as a squadMember
    @CrossOrigin
    @PostMapping("/game/{game_id}/squad")
    public ResponseEntity addSquad(@PathVariable(name = "game_id") int gameId, @RequestBody Squad squad, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        //if(!playerRepository.existsPlayerByGameIdAndUserId(gameId, Math.toIntExact(userPrincipal.getId())))

        Player player = playerRepository.findPlayerByGameIdAndUserId(gameId, Math.toIntExact(userPrincipal.getId()));
        if(player == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(squad_error);
        }

        if(!(playerRepository.findPlayerByGameIdAndUserId(gameId, Math.toIntExact(userPrincipal.getId())).isHuman)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(squad_error);
        }

        player = playerRepository.findPlayerByGameIdAndUserId(gameId, Math.toIntExact(userPrincipal.getId()));

        squadRepository.save(squad);

        SquadMember tempSquadMember = new SquadMember();
        tempSquadMember.id = 0;
        tempSquadMember.squadMemberRank = true;
        tempSquadMember.gameId = gameId;
        tempSquadMember.squadId = squad.id;

        player.squadId = squad.id;
        playerRepository.save(player);

        tempSquadMember.squadMember = player; // import file
        squadMemberRepository.save(tempSquadMember);

        Squad tempSquad = squadRepository.findSquadBygameIdAndId(gameId,squad.id);
        return ResponseEntity.status(HttpStatus.OK).body(tempSquad);
    }

    @CrossOrigin
    @PutMapping("/game/{game_id}/squad/{squad_id}")
    public ResponseEntity updateSquad(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId, @RequestBody Squad squad) {

        Squad prevSquad = squadRepository.findSquadBygameIdAndId(gameId, squadId);

        if (prevSquad == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(squad_error);
        }

        prevSquad.squadName = squad.squadName;
        return ResponseEntity.status(HttpStatus.OK).body(squadRepository.save(prevSquad));
    }

    @CrossOrigin
    @DeleteMapping("/game/{game_id}/squad/{squad_id}")
    public ResponseEntity deleteSquad(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId, @RequestBody Squad squad) {
        Squad squad1 = squadRepository.findSquadBygameIdAndId(gameId,squad.id);
        squadRepository.delete(squad);
        return ResponseEntity.status(HttpStatus.OK).body(squad1);
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/squad/{squad_id}/chat")
    public ResponseEntity getSquadChat(@PathVariable int game_id, @PathVariable int squad_id, Authentication authentication){
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Player player = playerRepository.findPlayerByGameIdAndUserId(game_id, Math.toIntExact(userPrincipal.getId()));

        if (player == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(squad_error);
        }

        if(!player.isHuman) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(squad_error);
        }

        if (player.squadId == 0 ) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(squad_error);
        }

        ArrayList<Chat> chat = chatRepository.findChatByGameIDAndSquadId(game_id, player.squadId);


        //DONE do logic for squad chat.
        //DONE also add squad_id to chat model.

        return ResponseEntity.status(HttpStatus.OK).body(chat);
    }

    @CrossOrigin
    @PostMapping("/game/{game_id}/squad/{squad_id}/chat")
    public ResponseEntity sendSquadChat(@RequestBody Chat chat, @PathVariable int game_id, @PathVariable int squad_id, Authentication authentication){
        chat.isZombie=false;
        chat.isHuman=false;

        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();

        if (!playerRepository.existsPlayerByGameIdAndUserId(game_id, Math.toIntExact(principal.getId()))) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(squad_error);
        }

        String username = principal.getUsername();
        chat.username = username;
        Player p = playerRepository.findPlayerByGameIdAndUserId(game_id, Math.toIntExact(principal.getId()));
        chat.playerId = p.id;
        LocalTime now = LocalTime.now();
        chat.sqlTime = Time.valueOf(now);

        if(squad_id==0||p.squadId!=squad_id){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(squad_error);
        }


        chat.squadId = squad_id;
        chat.gameID = game_id;

        boolean isAdmin = Arrays.toString(authentication.getAuthorities().toArray()).equals("[ROLE_ADMIN]");
        boolean isInSquad = playerRepository.existsBySquadIdAndUsername(chat.squadId, username);
        boolean isHuman = playerRepository.findPlayerBySquadIdAndUsername(chat.squadId, username).isHuman;
        if(isAdmin){
            chatRepository.save(chat);
            return ResponseEntity.ok(chat);
        }else if(isInSquad&&isHuman){
            chatRepository.save(chat);
            return ResponseEntity.ok(chat);
        }else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(squad_error);
        }
    }
}


