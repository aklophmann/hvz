package no.noroff.HvZ.controller;

import no.noroff.HvZ.model.Player;
import no.noroff.HvZ.model.SquadCheckIn;
import no.noroff.HvZ.repository.PlayerRepository;
import no.noroff.HvZ.repository.SquadCheckInRepository;
import no.noroff.HvZ.repository.SquadRepository;
import no.noroff.HvZ.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class SquadCheckInController {

    @Autowired
    private final SquadCheckInRepository squadCheckInRepository;

    @Autowired
    private final PlayerRepository playerRepository;

    @Autowired
    private final SquadRepository squadRepository;

    private final SquadCheckIn squadCheckIn_error = new SquadCheckIn();

    public SquadCheckInController(SquadCheckInRepository squadCheckInRepository, PlayerRepository playerRepository, SquadRepository squadRepository) {
        this.squadCheckInRepository = squadCheckInRepository;
        this.playerRepository = playerRepository;
        this.squadRepository =  squadRepository;
    }

    @CrossOrigin
    @GetMapping("/game/{game_id}/squad/{squad_id}/check-in")
    public ResponseEntity getSquadCheckIn(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId, Authentication authentication) {
        //DONE:  Allow only admin and active squad members to access
        List<SquadCheckIn> sci = squadCheckInRepository.findByGameIdAndSquadId(gameId, squadId);
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        String username = principal.getUsername();
        boolean is_admin = Arrays.toString(authentication.getAuthorities().toArray()).equals("[ROLE_ADMIN]");
        boolean isInSquad = playerRepository.existsBySquadIdAndUsername(squadId,username);
        Player p = null;
        if(isInSquad){
            p = playerRepository.findPlayerBySquadIdAndUsername(squadId,username);
        }
        if (sci.size() > 0){
            if(is_admin){
                return ResponseEntity.ok(sci);
            }else if(isInSquad){
                if(p.isHuman) return ResponseEntity.ok(sci);
            }else{
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(squadCheckIn_error);
            }

        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(squadCheckIn_error);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/game/{game_id}/squad/{squad_id}/check-in")
    public ResponseEntity addSquadCheckIn(@PathVariable(name = "game_id") int gameId, @PathVariable(name = "squad_id") int squadId, @RequestBody SquadCheckIn squadCheckIn, Authentication authentication) {
        //DONE: Allow only admin and active squad members to post (non-dead/non-zombie)
        int id = squadCheckIn.squadId;
        int game_id = squadCheckIn.gameId;
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        String username = principal.getUsername();
        boolean isAdmin = Arrays.toString(authentication.getAuthorities().toArray()).equals("[ROLE_ADMIN]");
        boolean isInSquad = playerRepository.existsBySquadIdAndUsername(id, username);

        if(isAdmin){
            squadCheckInRepository.save(squadCheckIn);
            return ResponseEntity.ok(squadCheckIn);
        }else if(isInSquad){
            squadCheckInRepository.save(squadCheckIn);
            return ResponseEntity.ok(squadCheckIn);
        }else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(squadCheckIn_error);
        }
    }
}
