package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.SquadCheckIn;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SquadCheckInRepository extends JpaRepository<SquadCheckIn, Integer> {
    SquadCheckIn findSquadCheckInById(int id);
    List<SquadCheckIn> findByGameIdAndSquadId(int game_id, int squad_id);
}