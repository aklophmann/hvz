package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Integer> {

    Player findPlayerById(int id);

    ArrayList<Player> findPlayerBygameId(int game_id);

    Player findBygameIdAndId(int game_id, int player_id);

    Player findPlayerByGameIdAndUserId(int game_id, int user_id);

    Player findPlayerByBiteCode(int bite_code);

    boolean existsByBiteCode(int bite_code);

    boolean existsBySquadIdAndUsername(int squadId, String username);

    Player findPlayerBySquadIdAndUsername(int squadId, String username);

    Player findPlayerByUsername(String username);

    boolean existsPlayerByGameIdAndUserId(int gameId, int userId);
}
