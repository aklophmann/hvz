package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserById(int id);
    // User findByUsername(String username);
    User findByUsername(String username);
    boolean existsByUsername(String username);
}