package no.noroff.HvZ.repository;

import no.noroff.HvZ.model.Mission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MissionRepository extends JpaRepository<Mission, Integer> {
    Mission findMissionById(int id);

    List<Mission> findMissionBygameId(int id);
}