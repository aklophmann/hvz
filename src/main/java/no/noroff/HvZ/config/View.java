package no.noroff.HvZ.config;

public class View {
    public interface Anonymous {}

    public interface User extends Anonymous {}

    public interface Admin extends User {}

    public interface CurrentUser extends User{}

}
